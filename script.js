(async function () {
    const albumUrl = "https://jsonplaceholder.typicode.com/albums"
    const photosUrl = "https://jsonplaceholder.typicode.com/photos"

    async function loadAlbums(limit = 5) {
        const resp = await fetch(`${albumUrl}?_limit=${limit}`)
        return await resp.json()
    }

    async function loadPhotos(albumId, limit = 5) {
        const resp = await fetch(`${photosUrl}?_limit=${limit}&albumId=${albumId}`)
        return resp.json()
    }

    $('#albumsCount').change(async function () {
        $('#photosPreloader').css("display", "flex").fadeIn()
        const count = $(this).val()
        renderAlbums(await loadAlbums(count))
        renderPhotos([])
        $('#photosPreloader').fadeOut()
    })

    async function onAlbumClick() {
        $('#photosPreloader').css("display", "flex").fadeIn()
        const ALBUM_ID = $(this).data('id')
        const countPhotos = $('#photosCount').val()
        renderPhotos(await loadPhotos(ALBUM_ID, countPhotos))
        $('#photosCount').change(async function () {
            $('#photosPreloader').fadeIn()
            const count = $(this).val()
            renderPhotos(await loadPhotos(ALBUM_ID, count))
            $('#photosPreloader').fadeOut()
        })
        $('#photosPreloader').fadeOut()
    }

    function renderPhotos(photos = []) {
        $('#photos').empty()
        for (const photo of photos) {
            $('#photos').append(`<div class="col-3">
                                        <div class="card" data-photo-id="${photo.id}">
                                             <img alt="${photo.title}" class="card-img-top" src="${photo.thumbnailUrl}">
                                        </div>
                                     </div>`)
        }
    }

    function renderAlbums(albums = []) {
        $('#albumsPreloader').css("display", "flex").fadeIn()
        $('#albums').empty()
        for (const album of albums) {
            $('#albums').append(`<li class="list-group-item" data-id="${album.id}">
                                                ${album.title}
                    </li>`)
            $('.list-group-item').click(onAlbumClick)
        }
        $('#albumsPreloader').fadeOut()
    }

    renderAlbums(await loadAlbums())
    $('#preloader').fadeOut()
})()